// SPDX-License-Identifier: GPL-3.0-or-later
use gettextrs::*;
use gtk::{gio, glib};

mod application;
mod config;
mod database;
mod model;
mod qrcode;
mod qrcode_kind;
mod screenshot;
mod utils;
mod widgets;

fn main() -> glib::ExitCode {
    tracing_subscriber::fmt::init();

    glib::set_application_name(&gettext("Decoder"));

    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR).expect("Could not bind text domain");
    textdomain(config::GETTEXT_PACKAGE).expect("Could not set text domain");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/decoder.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    application::Application::run()
}
