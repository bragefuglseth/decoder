// SPDX-License-Identifier: GPL-3.0-or-later
use gtk::{glib, subclass::prelude::*};

mod imp {
    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(resource = "/com/belmoussaoui/Decoder/ui/qrcode_kind_telephone.ui")]
    pub struct QRCodeTelephone {
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QRCodeTelephone {
        const NAME: &'static str = "QRCodeTelephone";
        type Type = super::QRCodeTelephone;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }
    impl ObjectImpl for QRCodeTelephone {}
    impl WidgetImpl for QRCodeTelephone {}
    impl BoxImpl for QRCodeTelephone {}
}

glib::wrapper! {
    pub struct QRCodeTelephone(ObjectSubclass<imp::QRCodeTelephone>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeTelephone {
    pub fn new(phone: String) -> Self {
        let widget = glib::Object::new::<QRCodeTelephone>();
        widget.init(phone);
        widget
    }

    fn init(&self, phone: String) {
        let escaped_phone = glib::markup_escape_text(&phone);

        self.imp().label.set_markup(&format!(
            "<a href='tel:{}'>{}</a>",
            escaped_phone, escaped_phone
        ));
    }
}
