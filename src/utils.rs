use gtk::gio;
use gtk::prelude::*;
use image::GenericImageView;
use zbar_rust::ZBarImageScanner;

pub fn linkify(input: &str) -> String {
    let finder = linkify::LinkFinder::new();

    finder.spans(input).fold(String::new(), |acc, span| {
        let append = match span.kind() {
            Some(&linkify::LinkKind::Url) => {
                format!(r"<a href='{}'>{}</a>", span.as_str(), span.as_str())
            }
            Some(&linkify::LinkKind::Email) => {
                format!(r"<a href='mailto:{}'>{}</a>", span.as_str(), span.as_str())
            }
            None => span.as_str().to_owned(),
            _ => span.as_str().to_owned(),
        };
        acc + &append
    })
}

pub fn scan(screenshot: &gio::File) -> anyhow::Result<String> {
    let (data, _) = screenshot.load_contents(gio::Cancellable::NONE)?;

    scan_bytes(&data)
}

pub fn scan_bytes(data: &[u8]) -> anyhow::Result<String> {
    let img = image::load_from_memory(data)?;

    let (width, height) = img.dimensions();
    let img_data: Vec<u8> = img.to_luma8().into_raw();

    let mut scanner = ZBarImageScanner::new();

    let mut results = scanner
        .scan_y800(&img_data, width, height)
        .map_err(anyhow::Error::msg)?;

    if results.is_empty() {
        anyhow::bail!("Invalid QR code")
    }

    let result = results.remove(0);
    let content = String::from_utf8(result.data)?;

    Ok(content)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn linkifi_str() {
        let uri = "hello upi://pay";
        let link = linkify(uri);
        assert_eq!(link, "hello <a href='upi://pay'>upi://pay</a>");
    }
}
